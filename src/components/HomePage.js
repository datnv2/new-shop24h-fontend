import React from "react";
import ProductList from "./content/ProductList";
import Footer from "./footer/Footer";
import Header from "./header/Header";
export const HomePage = () => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
      }}
    >
      <Header />
      <ProductList />
      <Footer />
    </div>
  );
};
export default HomePage;
