import axios from "axios";
import React, { useEffect, useState } from "react";
import { Container, Modal } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import {
  decrementProduct,
  deleteAllProduct,
  incrementProduct,
  removeProduct,
} from "../../actions/cart";
import Footer from "../footer/Footer";
import Header from "./Header";
export const ShoppingCart = (props) => {
  const history = useHistory();
  const productInfo = useSelector((state) => state.cart.listproduct);
  const dispatch = useDispatch();

  // hàm này thực hiện việc xóa sp trong giỏ hàng theo id được click
  const onBtnDeleteProduct = (id) => {
    const action = removeProduct(id);
    dispatch(action);
  };
  const [orderId, setOrderId] = useState("");
  const [showCreated, setShowCreated] = useState(false);
  //hàm này thực hiện đóng modal khi tạo đơn hàng thành công
  const handleCloseModalCreate = () => {
    setShowCreated(false);
  };
  //set trạng thái modal sẽ hiển thị khi bấm xóa sp trong đơn hàng
  const [show, setShow] = useState(false);
  const [showProduct, setShowProduct] = useState(false);
  const [modalProductState, setModalProductState] = useState(false);
  const [modalState, setModalState] = useState(true);
  const [productEnd, setProductEnd] = useState();
  //hàm này thực hiện đóng modal lại
  const handleClose = () => setShow(false);
  const [note, setNote] = useState("");
  //hàm thực hiện hiển thị modal tạo order sp lên trong giỏ hàng
  const handleCloseProduct = () => {
    setShowProduct(false);
    history.push("/");
  };

  //check nếu ko có thông tin của user thì khi user click vào giỏ hàng sẽ chuyển ra màn hình đăng nhập
  useEffect(() => {
    if (localStorage.name === undefined || localStorage.name === "") {
      history.push("/login");
    }
  });

  //chuyển đổi thành số có dấu phẩy
  let decimalNumber = new Intl.NumberFormat("en-US");
  //khai báo kết quả của giỏ hàng là 1 mảng các sản phẩm
  let result = [];
  //thêm các phẩm cùng với giá và số lượng vào mảng sản phẩm
  productInfo.map((data) =>
    result.push(data.product.priceSale * data.product.productQuantity)
  );

  // thực hiện tính tiền khi click mua sp theo số lượng
  let totalPrice = result.reduce(function (acc, val) {
    return acc + val;
  }, 0);

  // hàm này để thực hiện giảm số lượng sp trong giỏ hàng xuống
  const onDecrementClick = (product) => {
    if (product.product.productQuantity > 1) {
      const action = decrementProduct(product);
      dispatch(action);
    } else {
      setShow(true);
      setProductEnd(product.idPro);
    }
  };

  // hàm này để thực hiện tăng số lượng sp trong giỏ hàng xuống
  const onIncrementClick = (product) => {
    const action = incrementProduct(product);
    dispatch(action);
  };

  // hàm này thực hiện việc kiểm tra xem user bấm xóa 1 sp hay hóa toàn bộ sp
  const handleRemoveEndItem = () => {
    if (modalState) {
      // delete item
      const action = removeProduct(productEnd);
      dispatch(action);
      setShow(false);
    } else {
      //delete all
      const action = deleteAllProduct(productInfo);
      dispatch(action);
      setShow(false);
    }
  };

  // hàm này thực hiện xóa toàn bộ sp trong giỏ hàng
  const onDeleteAll = () => {
    if (productInfo.length > 0) {
      setShow(true);
      setModalState(false);
      setModalProductState(false);
    } else {
      return;
    }
  };

  // hàm này thực hiện việc tạo đơn hàng
  const onBuyNowClick = () => {
    if (productInfo.length === 0) {
      setShowProduct(true);
    } else {
      setModalProductState(true);
      setShowProduct(true);
    }
  };
  //khai báo biến ngày tháng năm
  let today = new Date();
  let date =
    today.getDate() +
    "/" +
    parseInt(today.getMonth() + 1) +
    "/" +
    today.getFullYear();

  // hàm này thực hiện xác nhận và gọi api để tạo 1 đơn hàng mới
  const onBtnCreateOrderClick = async () => {
    //thông tin gửi kèm khi gọi api tạo đơn
    const orderObj = {
      customerId: localStorage.id,
      orderDate: today,
      requiredDate: today,
      shippedDate: today,
      note: note,
    };
    //gọi api tạo đơn
    const createOrder = await axios.post(
      "http://localhost:5000/order",
      orderObj
    );
    //nếu thành công thì sẽ tạo 1 orderid trên csdl
    if (createOrder.data.success) {
      setOrderId(createOrder.data.Order._id);
      setShowProduct(false);
      setShowCreated(true);
    }
  };

  // hàm này thực hiện việc gọi api để check chi tiết đơn hàng
  const onBtnSuccessCreateOrder = async () => {
    var productLength = productInfo.length;
    for (var bI = 0; bI < productLength; bI++) {
      const orderDetailObject = {
        orderId: orderId,
        productId: productInfo[bI].product._id,
        quantity: productInfo[bI].product.productQuantity,
        priceEach: productInfo[bI].product.productPrice,
      };
      const orderDetail = await axios.post(
        "http://localhost:5000/order-detail",
        orderDetailObject
      );
      if (orderDetail.data.success) {
        setShowCreated(false);
        history.push("/");
      }
    }
  };
  return (
    <div className="container-body-cart">
      <Header />
      <Container /* style={{ marginTop: 200 }} */>
        <div className="cart-container">
          <div className="cart-inner">
            <h4>Shopping Cart</h4>
            <div className="cart-content">
              <div className="cart-content-header">
                <div className="cart-content-header-name">Tên sản phẩm</div>
                <div className="cart-content-header-price">Giá bán</div>
                <div className="cart-content-header-quantity">Số lượng</div>
                <div className="cart-content-header-money">Thành tiền</div>
                <div
                  className="cart-content-header-delete"
                  onClick={onDeleteAll}
                >
                  <i className="fas fa-trash" />
                </div>
              </div>
            </div>
            <div className="cart-content">
              {productInfo.map((element, index) => (
                <div key={index} className="cart-content-header">
                  <div className="cart-content-header-name">
                    {element.product.productName}
                  </div>
                  <div className="cart-content-header-price">
                    {decimalNumber.format(element.product.productPrice)}
                  </div>
                  <div className="cart-content-header-quantity">
                    <button
                      className="icon-remove"
                      onClick={() => onDecrementClick(element)}
                    >
                      <span className="icon-remove-minus fas fa-minus-circle" />
                    </button>
                    <span>{element.product.productQuantity}</span>
                    <button
                      className="icon-add"
                      onClick={() => onIncrementClick(element)}
                    >
                      <span class="icon-remove-plus fas fa-plus-circle" />
                    </button>
                  </div>
                  <div className="cart-content-header-money">
                    {decimalNumber.format(
                      element.product.priceSale *
                        element.product.productQuantity
                    )}
                  </div>
                  <div
                    className="cart-content-header-delete"
                    onClick={() => onBtnDeleteProduct(element.idPro)}
                  >
                    <span className="fas fa-trash" />
                  </div>
                </div>
              ))}
            </div>
          </div>
          <div className="cart-user">
            <div className="cart-user-info">
              <div className="cart-user-info-header">
                <div className="cart-user-info-address">
                  Thông tin giao hàng
                  <i className="fas fa-truck" style={{ margin: 5 }} />
                </div>
                {/* //nếu user cần thay đổi thông tin cá nhân thì có thể bấm vào để về trang cập nhật user */}
                <Link to="/customer-info">
                  <div className="cart-user-info-change">Thay đổi địa chỉ</div>
                </Link>
              </div>
              <div className="cart-user-info-name-phone">
                <div className="cart-user-info-name">
                  <span>Họ tên: {localStorage.name}</span>
                </div>
              </div>

              <div className="cart-user-info-phone">
                <span>Số điện thoại: {localStorage.phone}</span>
              </div>
              <div className="cart-user-info-address">
                <span>Địa chỉ: {localStorage.address}</span>
              </div>
            </div>

            <div className="cart-user-info2">
              <div className="cart-user-info-header">
                <div className="cart-user-info-address">Tổng số sản phẩm</div>
                <div className="cart-user-info-change">
                  {productInfo.length}
                </div>
              </div>
              <div className="cart-user-info-header">
                <div className="cart-user-info-total">Tổng tiền</div>
                <div className="cart-user-info-price">
                  {decimalNumber.format(totalPrice)}
                  <span> VND</span>
                </div>
              </div>
            </div>

            <button className="btn-buy-now" onClick={onBuyNowClick}>
              Tiếp tục mua hàng
            </button>
          </div>
        </div>
      </Container>
      <Footer />
      {/* modal 1 start */}
      <Modal show={show} onHide={handleClose} animation={true}>
        <Modal.Header closeButton>
          <Modal.Title>Thông báo</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {modalState
            ? "Bạn có muốn xóa sản phẩm này không?"
            : "Bạn muốn xóa toàn bộ sản phẩm ở giỏ hàng? "}
        </Modal.Body>
        <Modal.Footer>
          <button className="btn-cancel" onClick={handleClose}>
            Đóng
          </button>
          <button className="btn-update" onClick={handleRemoveEndItem}>
            Xóa sản phẩm
          </button>
        </Modal.Footer>
      </Modal>
      {/* modal 1 end */}

      {/* modal 2 start */}
      <Modal
        size={modalProductState ? "lg" : "md"}
        show={showProduct}
        onHide={handleCloseProduct}
        animation={true}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            {modalProductState ? "Chi tiết đơn hàng" : "Thông báo"}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {modalProductState ? (
            <div className="modal-content-order">
              <div className="content-product">
                {productInfo.map((element, index) => (
                  <div key={index} className="content-product-body">
                    <div className="content-product-img">
                      <img alt="" src={element.product.productImage[0]} />
                    </div>
                    <div className="content-product-name">
                      {element.product.productName}
                    </div>
                    <div className="content-product-quantity">
                      {element.product.productQuantity}
                    </div>
                    <div className="content-product-money">
                      {decimalNumber.format(
                        element.product.priceSale *
                          element.product.productQuantity
                      )}
                    </div>
                  </div>
                ))}
              </div>
              <div className="content-user">
                <label>Full name: </label>
                <div className="info-user">
                  <p>{localStorage.name}</p>
                </div>
                <label>Address:</label>
                <div className="info-order">
                  <p>{localStorage.address}</p>
                </div>
                <label>Phone number:</label>
                <div className="info-phone">
                  <p>{localStorage.phone}</p>
                </div>
                <label>Email:</label>
                <div className="info-email">
                  <p>{localStorage.email}</p>
                </div>
                <label>Note:</label>
                <div>
                  <textarea
                    onChange={(e) => {
                      setNote(e.target.value);
                    }}
                    className="info-note"
                  ></textarea>
                </div>
                <label className="info-shipped">Shipped date:</label>
                <div className="info-shipper">
                  <p>{date}</p>
                </div>
              </div>
            </div>
          ) : (
            "Không có sản phẩm nào trong giỏ hàng để tạo đơn"
          )}
        </Modal.Body>
        <Modal.Footer>
          {modalProductState ? (
            <button className="btn-cancel" onClick={handleCloseProduct}>
              Đóng
            </button>
          ) : (
            ""
          )}
          <button
            className="btn-update"
            onClick={
              modalProductState ? onBtnCreateOrderClick : handleCloseProduct
            }
          >
            {modalProductState ? "Tạo đơn" : "Cảm ơn"}
          </button>
        </Modal.Footer>
      </Modal>
      {/* modal 2 end */}

      {/* modal created order */}
      <Modal
        show={showCreated}
        onHide={handleCloseModalCreate}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Thông báo</Modal.Title>
        </Modal.Header>
        <Modal.Body>Đã tạo đơn hàng thành công</Modal.Body>
        <Modal.Footer>
          <button className="btn-update" onClick={onBtnSuccessCreateOrder}>
            Đóng
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};
export default ShoppingCart;
