import React, { useState } from "react";

import { useHistory } from "react-router-dom";
import SearchIcon from "@mui/icons-material/Search";
export const SearchProduct = () => {
  const history = useHistory();
  const [inputValue, setInputValue] = useState("all");
  const [selectValue, setSelectValue] = useState("all");

  // hàm này thực hiện việc chuyển hướng tìm sp theo value nhập vào trên input
  const btnSearchClick = () => {
    history.push("/products/" + selectValue + "/" + inputValue);
  };

  // hàm này thực hiện việc thay đổi giá trị ô input theo value được chọn
  const handleChangeValueInput = (e) => {
    if (e.target.value === "") {
      setInputValue("all");
    } else {
      setInputValue(e.target.value);
    }
  };

  return (
    <div className="search-container">
      <div className="form-container">
        <select
          className="form-select"
          onChange={(e) => setSelectValue(e.target.value)}
        >
          <option value="all">all</option>
          <option value="name">name</option>
          <option value="pricemin">pricemin</option>
          <option value="pricemax">pricemax</option>
        </select>

        <input
          type="text"
          className="form-input"
          onChange={(e) => handleChangeValueInput(e)}
        />
        <div className="form-border"></div>
        <div className="form-icon">
          <SearchIcon
            fontSize="large"
            className="icon-search"
            onClick={btnSearchClick}
          />
        </div>
      </div>
    </div>
  );
};
export default SearchProduct;
