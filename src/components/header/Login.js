import React, { useState } from "react";
import { Container } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import { signInWithGoogle } from "../../Firebase/Config";

export const Login = () => {
  const history = useHistory();
  const [errState, setErrState] = useState(false);
  const [email, setEmail] = useState("");

  // hàm này gọi api để check xem email đã có trong database chưa để đăng nhập user
  const btnContinueClick = async () => {
    if (email !== "") {
      const login = await axios.get(
        "http://localhost:5000/customer-login?login=" + email
      );
      //nếu có thông tin ở trang login thì lấy từ csdl ra và hiển thị lên localStorage
      if (login.data.customer !== null) {
        const name = login.data.customer.fullName;
        const emailLogin = login.data.customer.email;
        const profilePic = login.data.customer.photoURL;
        const address = login.data.customer.address;
        const phone = login.data.customer.phone;
        const id = login.data.customer._id;
        localStorage.setItem("id", id);
        localStorage.setItem("name", name);
        localStorage.setItem("email", emailLogin);
        localStorage.setItem("profilePic", profilePic);
        localStorage.setItem("phone", phone);
        localStorage.setItem("address", address);
        history.push("/");
      } else {
        setErrState(true);
      }
    } else {
      setErrState(true);
    }
  };

  // hàm này thực hiện việc quay lại trang chủ khi click
  const comeBackHome = () => {
    history.push("/");
  };
  return (
    <Container className="login-container">
      <div className="shop-name">
        <h2>Đăng nhập tài khoản </h2>
      </div>
      <div className="form-login">
        <h3>Đăng nhập</h3>
        <p>Đăng nhập bằng email hoặc số điện thoại</p>
        <input
          onChange={(e) => setEmail(e.target.value)}
          placeholder="nhập email hoặc số điện thoại ..."
        />
        <span className={errState ? "span-err" : "span-hide"}>
          email hoặc số điện thoại không tồn tại. Vui lòng đăng ký tài khoản
        </span>
        <button className="btn-login" onClick={btnContinueClick}>
          Đăng nhập
        </button>
        <div
          id="customBtn"
          className="customGPlusSignIn"
          onClick={signInWithGoogle}
        >
          <span className="icon"></span>
          <span className="buttonText">Đăng nhập Google</span>
        </div>
      </div>

      <div className="create-account">
        <Link to="/create-account">
          <button style={{ marginTop: 10 }}>Tạo tài khoản</button>
        </Link>
      </div>
      <div className="back-home">
        <button style={{ marginTop: 10 }} onClick={comeBackHome}>
          Quay lại
        </button>
      </div>
    </Container>
  );
};
export default Login;
