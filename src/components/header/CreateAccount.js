import axios from "axios";
import React, { useState } from "react";
import { Container, Modal } from "react-bootstrap";
import { useHistory } from "react-router-dom";
export const CreateAccount = () => {
  // lấy thông tin fullName  của user dựa vào localStorage
  const [fullName, setFullName] = useState(localStorage.name);
  // lấy thông tin email  của user dựa vào localStorage
  const [email, setEmail] = useState(localStorage.email);
  //ban đầu giá trị các ô input là trống
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [avatar, setAvatar] = useState("");
  const [show, setShow] = useState(false);
  // hàm thực hiện việc đóng modal lại
  const handleClose = () => {
    setShow(false);
  };
  // cảnh báo khi user để số điện thoại trống
  const [phoneErr, setPhoneErr] = useState(false);
  // cảnh báo khi địa chỉ để số điện thoại trống
  const [addressErr, setAddressErr] = useState(false);
  const history = useHistory();
  // modal hiển thị thông báo khi tạo user mới
  const [modalCheck, setModalCheck] = useState(false);

  // hàm này gọi api để người dùng tạo mới tài khoản và check xem email hay phone đã có trong database chưa
  const checkPhoneEmailAndCreateUserAccount = async () => {
    //nếu mà phone hoặc email mà đã có thì gọi api để ktra trong database
    if (phone !== "" && address !== "") {
      const checkPhone = await axios.get(
        "http://localhost:5000/customer-phone?phone=" + phone
      );
      const checkEmail = await axios.get(
        "http://localhost:5000/customer-email?email=" + email
      );
      // nếu kiểm tra mà chưa có email và số điện thoại trong database thì gọi api để tạo mới cho người dùng
      if (
        checkPhone.data.customer === null &&
        checkEmail.data.customer === null
      ) {
        const customer = {
          fullName: fullName,
          phone: phone,
          email: email,
          photoURL: avatar,
          address: address,
        };

        // gọi api để tạo tk
        const create = await axios.post(
          "http://localhost:5000/customer",
          customer
        );
        setModalCheck(true);

        // nếu tạo mới user thành công thì mở modal thông báo lên
        if (create.data.customer !== null) {
          setShow(true);
        }
      } else {
        setShow(true);
      }
    } else {
      setPhoneErr(true);
      setAddressErr(true);
    }
  };

  // hàm thực hiện quay về trang chủ
  const backToLogin = () => {
    history.push("/login");
  };

  return (
    <Container className="container-create">
      <div className="title-shop">
        <h1>Tạo tài khoản với Shop24h</h1>
      </div>
      <div className="form-create-account">
        <h3>Tạo tài khoản</h3>
        <label>Tên</label>
        <input
          type="text"
          defaultValue={fullName}
          onChange={(e) => setFullName(e.target.value)}
        />
        <label>Email</label>
        <input
          type="text"
          defaultValue={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <label>Số điện thoại</label>
        <input
          type="text"
          defaultValue={phone}
          onChange={(e) => setPhone(e.target.value)}
        />
        <span className={phoneErr ? "show-err" : "hide-err"}>
          số điện thoại không được để trống
        </span>
        <label>Địa chỉ</label>
        <input
          type="text"
          defaultValue={address}
          onChange={(e) => setAddress(e.target.value)}
        />
        <span className={addressErr ? "show-err" : "hide-err"}>
          địa chỉ không được để trống
        </span>
        <label>Ảnh đại diện (nếu có) </label>
        <input
          type="text"
          defaultValue={avatar}
          onChange={(e) => setAvatar(e.target.value)}
        />
        <button onClick={checkPhoneEmailAndCreateUserAccount}>
          Tạo tài khoản
        </button>
        <button onClick={backToLogin}>Quay lại</button>
        {/* <Link to="/">
          <button style={{ marginLeft: 120 }}>Home</button>
        </Link> */}
      </div>
      <Modal show={show} onHide={handleClose} animation={true}>
        <Modal.Header closeButton>
          <Modal.Title>Thông báo</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {modalCheck
            ? "Đã tạo tài khoản thành công"
            : "Tạo tài khoản thất bại"}
        </Modal.Body>
        <Modal.Footer>
          <button className="btn-update" onClick={handleClose}>
            Đóng
          </button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
};

export default CreateAccount;
