import axios from "axios";
import React, { useState } from "react";
import { Container, Modal } from "react-bootstrap";
import { useHistory } from "react-router-dom";
export const CustomerInfo = () => {
  const history = useHistory();
  const [errMessage, setErrMessage] = useState("");
  const [show, setShow] = useState(false);

  // hàm thực hiện đóng modal lại
  const handleClose = () => {
    setShow(false);
  };

  // hàm thực hiện mở modal lên
  const handleShow = () => {
    setShow(true);
  };

  //set phần profile của user là 1 đối tượng và lưu trên localStorage
  const [profile, setProfile] = useState({
    fullName: localStorage.name,
    email: localStorage.email,
    phone: localStorage.phone,
    address: localStorage.address,
    photoURL: localStorage.profilePic,
  });

  // hàm này thực hiện việc nếu user ko muốn update thì khi bấm quay về trang chủ
  const handleCancelUpdate = () => {
    history.push("/");
  };

  // hàm này thực hiện việc update thông tin của user
  const handleUpdateClick = async () => {
    // nếu profile user update mà trùng thông tin với data có trên localStorage thì tiến hành gọi api để update thông tin user
    if (
      profile.email === localStorage.email &&
      profile.phone === localStorage.phone
    ) {
      const update = await axios.put(
        "http://localhost:5000/customer/update/" +
          profile.email +
          "/" +
          profile.phone,
        profile
      );
      // nếu update thành công thì xóa thông tin cũ và update lại thông tin mới cho user
      if (update.data.success) {
        localStorage.removeItem("name");
        localStorage.removeItem("email");
        localStorage.removeItem("phone");
        localStorage.removeItem("address");
        localStorage.removeItem("profilePic");
        localStorage.setItem("name", update.data.customer.fullName);
        localStorage.setItem("email", update.data.customer.email);
        localStorage.setItem("phone", update.data.customer.phone);
        localStorage.setItem("address", update.data.customer.address);
        localStorage.setItem("profilePic", update.data.customer.photoURL);
        setErrMessage("cập nhật thành công");
        handleShow();
      }
      //còn nếu email trong thông tin của user trùng với email trên localStorage thì lấy thông tin ra
    } else if (profile.email === localStorage.email) {
      const checkPhone = await axios.get(
        "http://localhost:5000/customer-phone?phone=" + profile.phone
      );
      // nếu data user kiểm tra theo số điện thoai hoặc email mà null thì gọi api để user cập nhật thông tin theo pfofile
      if (checkPhone.data.customer === null) {
        const updateEmail = await axios.put(
          "http://localhost:5000/customer/update-email?email=" + profile.email,
          profile
        );
        localStorage.removeItem("name");
        localStorage.removeItem("email");
        localStorage.removeItem("phone");
        localStorage.removeItem("address");
        localStorage.removeItem("profilePic");
        localStorage.setItem("name", updateEmail.data.customer.fullName);
        localStorage.setItem("email", updateEmail.data.customer.email);
        localStorage.setItem("phone", updateEmail.data.customer.phone);
        localStorage.setItem("address", updateEmail.data.customer.address);
        localStorage.setItem("profilePic", updateEmail.data.customer.photoURL);

        setErrMessage("cập nhật thành công");
        handleShow();
        //còn nếu email hoặc số đt cập nhật trùng với data đã tồn tại thì báo cập nhật thất bại
      } else {
        setErrMessage("cập nhật thất bại, số điện thoại đã tồn tại");
        handleShow();
      }
      //nếu phone trùng với phone trên localStorage thì lấy ra thông tin user
    } else if (profile.phone === localStorage.phone) {
      const checkEmail = await axios.get(
        "http://localhost:5000/customer-email?email=" + profile.email
      );
      //nếu check data mà null thì gọi api để user update thông tin và set lại data trên localStorage
      if (checkEmail.data.customer === null) {
        const updateEmail = await axios.put(
          "http://localhost:5000/customer/update-phone?phone=" + profile.phone,
          profile
        );
        localStorage.removeItem("name");
        localStorage.removeItem("email");
        localStorage.removeItem("phone");
        localStorage.removeItem("address");
        localStorage.removeItem("profilePic");
        localStorage.setItem("name", updateEmail.data.customer.fullName);
        localStorage.setItem("email", updateEmail.data.customer.email);
        localStorage.setItem("phone", updateEmail.data.customer.phone);
        localStorage.setItem("address", updateEmail.data.customer.address);
        localStorage.setItem("profilePic", updateEmail.data.customer.photoURL);

        setErrMessage("cập nhật thành công");
        handleShow();
      } else {
        setErrMessage("cập nhật thất bại email đã tồn tại");

        handleShow();
      }
    } else {
      const checkCustomer = await axios.get(
        "http://localhost:5000/customer-email-phone?email=" +
          profile.email +
          "&phone=" +
          profile.phone
      );
      if (checkCustomer.data.customer === null) {
        const updateCustomer = await axios.put(
          "http://localhost:5000/customer-update-email-or-phone?email=" +
            localStorage.email,
          profile
        );
        localStorage.removeItem("name");
        localStorage.removeItem("email");
        localStorage.removeItem("phone");
        localStorage.removeItem("address");
        localStorage.removeItem("profilePic");
        localStorage.setItem("name", updateCustomer.data.customer.fullName);
        localStorage.setItem("email", updateCustomer.data.customer.email);
        localStorage.setItem("phone", updateCustomer.data.customer.phone);
        localStorage.setItem("address", updateCustomer.data.customer.address);
        localStorage.setItem(
          "profilePic",
          updateCustomer.data.customer.photoURL
        );
        handleShow();

        setErrMessage("cập nhật thành công");
      }
    }
  };

  // hàm thực hiện hiển thị modal sau khi user cập nhật lại thông tin thành công và quay về trang chủ
  const handleSave = () => {
    handleShow();
    history.goBack();
  };
  return (
    <div>
      <Container>
        <div className="customer-container">
          <div className="customer-header">
            <h2 style={{ color: "blue", textAlign: "center" }}>
              Update Information User
            </h2>
          </div>
          <div className="customer-content">
            <div className="input-container">
              <input
                className="input-control"
                placeholder=" "
                defaultValue={profile.fullName}
                onChange={(e) =>
                  setProfile({ ...profile, fullName: e.target.value })
                }
              />
              <label className="form-label">Full name</label>
            </div>
            <div className="input-container">
              <input
                className="input-control"
                placeholder=" "
                defaultValue={profile.email}
                onChange={(e) =>
                  setProfile({ ...profile, email: e.target.value })
                }
              />
              <label className="form-label">Email</label>
            </div>
            <div className="input-container">
              <input
                className="input-control"
                placeholder=" "
                defaultValue={profile.phone}
                onChange={(e) =>
                  setProfile({ ...profile, phone: e.target.value })
                }
              />
              <label className="form-label">Phone number</label>
            </div>
            <div className="input-container">
              <input
                className="input-control"
                placeholder=" "
                defaultValue={profile.address}
                onChange={(e) =>
                  setProfile({ ...profile, address: e.target.value })
                }
              />
              <label className="form-label">Address</label>
            </div>
            <div className="input-container">
              <input
                className="input-control"
                placeholder=" "
                defaultValue={profile.photoURL}
                onChange={(e) =>
                  setProfile({ ...profile, photoURL: e.target.value })
                }
              />
              <label className="form-label">User Avatar</label>
            </div>
          </div>
          <div className="customer-bottom">
            <button className="btn-cancel" onClick={handleCancelUpdate}>
              Quay lại
            </button>
            <button className="btn-update" onClick={handleUpdateClick}>
              Cập nhật
            </button>
          </div>
        </div>
      </Container>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Thông báo</Modal.Title>
        </Modal.Header>
        <Modal.Body>{errMessage}</Modal.Body>
        <Modal.Footer>
          <button className="btn-cancel" onClick={handleClose}>
            Đóng
          </button>
          <button className="btn-update" onClick={handleSave}>
            Lưu lại
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};
export default CustomerInfo;
