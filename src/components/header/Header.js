import { Menu, MenuItem } from "@mui/material";
import "firebase/compat/auth";
import React, { useState } from "react";
import { Container, Image } from "react-bootstrap";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import avatar from "../../assets/avatar.jpg";
import { signOutWithGoogle } from "../../Firebase/Config";
import SearchProduct from "./SearchProduct";
export const Header = () => {
  const listProduct = useSelector((state) => state.cart.listproduct);

  //khai báo biến để hiển thị số lượng sp có trong giỏ hàng
  const itemProduct = listProduct.length;
  console.log(listProduct);
  const [anchorEl, setAnchorEl] = useState(null);
  //lấy biến chưa user có trên localStorage
  const username = localStorage.getItem("name");
  const open = Boolean(anchorEl);
  let defaultURL =
    "https://thumbs.dreamstime.com/z/default-avatar-profile-icon-social-media-user-vector-default-avatar-profile-icon-social-media-user-vector-portrait-176194876.jpg";

  // hàm này thực hiện việc thay đổi lựa chọn khi click trên thanh menu
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  // hàm này đóng menu lại
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div
      style={{ maxHeight: "200px", backgroundColor: "black" }}
      /*  className="fixed-top" */
    >
      <Container
        id="scroll"
        style={{ display: "flex", justifyContent: "space-around" }}
        className="container-search"
      >
        <div /* style={{ margin: "auto" }} */>
          <Link to="/">
            <Image
              style={{
                width: "100px",
                marginTop: 20,
                marginLeft: -10,
                marginRight: 10,
                marginBottom: 20,
                borderRadius: "10px",
              }}
              src={avatar}
            />
          </Link>
        </div>
        <div>
          <SearchProduct />
        </div>
        <div
          style={{
            filter: "brightness(1000%)",
            margin: "auto",
            fontSize: "20px",
            fontWeight: "bold",
            cursor: "pointer",
            display: "flex",
            justifyContent: "space-around",
          }}
          className="user-info"
          onClick={handleClick}
        >
          <span className="span-avatar">
            <img
              className="img-avatar"
              alt=""
              src={
                localStorage.profilePic ? localStorage.profilePic : defaultURL
              }
            />
          </span>
          <span className="span-avatar-name">
            {username !== null ? username : " Customer"}
          </span>
        </div>
        <div className="header-cart">
          <Link to="/shopping-cart">
            <i className="header-cart--shopping fas fa-shopping-cart" />

            <span className="header-cart-noti" style={{ marginLeft: 1 }}>
              {itemProduct}
            </span>
          </Link>
        </div>
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          MenuListProps={{
            "aria-labelledby": "basic-button",
          }}
        >
          <MenuItem>
            <Link to="/">Home</Link>
          </MenuItem>
          <MenuItem onClick={handleClose}>
            {username !== null ? (
              <Link to="/customer-info">Profile Users</Link>
            ) : (
              <Link to="/login">Login</Link>
            )}
          </MenuItem>
          <MenuItem onClick={handleClose}>
            {username !== null ? <Link to="/my-order">My Order</Link> : ""}
          </MenuItem>
          <MenuItem>
            <Link to="/login">Login</Link>
          </MenuItem>
          {/* <MenuItem>
            <Link to="/my-order">MyOrder</Link>
          </MenuItem> */}

          <MenuItem>
            <Link to="/create-account">Create Account</Link>
          </MenuItem>
          <MenuItem onClick={signOutWithGoogle}>Log out</MenuItem>
        </Menu>
      </Container>
    </div>
  );
};
export default Header;
