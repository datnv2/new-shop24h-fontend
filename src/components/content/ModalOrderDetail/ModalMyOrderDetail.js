import React, { useState, useEffect } from "react";
import { Modal, Button } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
} from "@mui/material";

export const ModalMyOrderDetail = ({
  show,
  orderDetail,
  handleClose,
  backHome,
}) => {
  const history = useHistory();

  //list product ban đầu là 1 mảng rỗng và sẽ gán lại sau khi gọi api
  const [productList, setProductList] = useState([]);

  // gọi api để load sản phẩm về và set sp vào mảng productList
  useEffect(() => {
    const getListProduct = async () => {
      const listProduct = await axios.get("http://localhost:5000/products");
      setProductList(listProduct.data.Product);
    };
    getListProduct();
  }, []);

  // hàm này để thay đổi sản phẩm trong mảng dựa vào id để trở về trang deltail sản phẩm
  const changeNameProduct = (productId) => {
    let result = [];
    result = productList.filter((prod) => {
      return prod._id === productId;
    });
    return result[0].productName;
  };

  // hàm này thực hiện việc khi user click mua lại thì điều hướng vào trang detail để add sp vào giỏ hàng
  const handleBuyAgain = (e) => {
    history.push("/detail/" + e.productId);
  };
  //đường dẫn để vào chi tiêt sp
  const detailUrl = "/detail/";
  return (
    <div>
      <Modal size="xl" show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title style={{ textAlign: "center", color: "blue" }}>
            Order Detail User
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Grid container>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      STT
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      OrderId
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      Product name
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      Quantity
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      Price
                    </TableCell>
                    {/* <TableCell
                    align="center"
                    style={{ border: "1px solid #f7e7ce" }}
                  >
                    Status
                  </TableCell> */}
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      Action
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody style={{ border: "1px solid #f7e7ce" }}>
                  {orderDetail.map((element, index) => (
                    <TableRow
                      key={index}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell
                        component="th"
                        scope="row"
                        align="center"
                        style={{ border: "1px solid #f7e7ce" }}
                      >
                        {index + 1}
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{ border: "1px solid #f7e7ce" }}
                      >
                        {element.orderId}
                      </TableCell>
                      <TableCell
                        style={{ border: "1px solid #f7e7ce" }}
                        align="center"
                      >
                        <Link to={detailUrl + element.productId}>
                          {changeNameProduct(element.productId)}
                        </Link>
                      </TableCell>
                      <TableCell
                        style={{ border: "1px solid #f7e7ce" }}
                        align="center"
                      >
                        {element.quantity}
                      </TableCell>
                      <TableCell
                        style={{ border: "1px solid #f7e7ce" }}
                        align="center"
                      >
                        {element.priceEach}
                      </TableCell>
                      {/* <TableCell
                      style={{ border: "1px solid #f7e7ce" }}
                      align="center"
                    >
                      {element.status}
                    </TableCell> */}
                      <TableCell align="center">
                        <Button
                          onClick={() => handleBuyAgain(element)}
                          variant="primary"
                          style={{ borderRadius: "10px" }}
                        >
                          Mua lại
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="danger"
            onClick={handleClose}
            style={{ borderRadius: "10px" }}
          >
            Đóng
          </Button>
          <Button
            variant="success"
            onClick={backHome}
            style={{ borderRadius: "10px" }}
          >
            <span class="fas fa-home"></span>
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};
export default ModalMyOrderDetail;
