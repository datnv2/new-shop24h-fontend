// import React, { useState, useEffect } from "react";
// import { Button, Carousel, Container } from "react-bootstrap";
// import { Link } from "react-router-dom";
// import axios from "axios";
import React from "react";
import products from "../../products.js";
import { Carousel, Container } from "react-bootstrap";
export const CarouselProduct = () => {
  // const [productList, setProductList] = useState([]);
  // useEffect(() => {
  //   const getProductList = async () => {
  //     try {
  //       const productDB = await axios.get(
  //         "http://localhost:5000/product-carousel"
  //       );
  //       setProductList(productDB.data.Product);
  //     } catch (err) {
  //       console.log(err.message);
  //     }
  //   };
  //   getProductList();
  // }, []);
  // const detailUrl = "/detail/";
  return (
    <div>
      <Container className="container-carousel" /* style={{ marginTop: 30 }} */>
        <Carousel className="carousel-control">
          <Carousel.Item>
            <img
              className="d-block "
              src={products[4].imageUrl}
              alt="First slide"
            />
            <Carousel.Caption>
              {/*    <h3> {products[8].name}</h3> */}
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block"
              src={products[1].imageUrl}
              alt="Second slide"
            />

            <Carousel.Caption></Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block "
              src={products[11].imageUrl}
              alt="Third slide"
            />

            <Carousel.Caption></Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block "
              src={products[3].imageUrl}
              alt="Fourd slide"
            />

            <Carousel.Caption></Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </Container>
    </div>

    // <Container style={{ marginBottom: "20px" }}>
    //   <Carousel>
    //     {productList.map((element, index) => (
    //       <Carousel.Item key={index}>
    //         <img
    //           className="d-block w-100"
    //           src={element.productImage}
    //           alt="First slide"
    //         />
    //         <Carousel.Caption
    //           style={{
    //             display: "flex",
    //             flexDirection: "column",
    //             textAlign: "left",
    //           }}
    //         >
    //           <h3>{element.productName}</h3>
    //           <p>{element.productDetail}</p>
    //           <Link to={detailUrl + element._id}>
    //             <Button style={{ width: "120px" }}>View</Button>
    //           </Link>
    //         </Carousel.Caption>
    //       </Carousel.Item>
    //     ))}
    //   </Carousel>

    // </Container>
  );
};
export default CarouselProduct;
