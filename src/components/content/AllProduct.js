import axios from "axios";
import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { Grid, Stack, Pagination } from "@mui/material";
import { useDispatch } from "react-redux";
import { Link, useParams, useHistory } from "react-router-dom";
import { addProduct } from "../../actions/cart";
import Header from "../../components/header/Header";
import Footer from "../footer/Footer";
export const AllProduct = () => {
  const [products, setProduct] = useState([]);
  const [productList, setProductList] = useState([]);

  // thực hiện phân trang
  // số trang bắt đầu từ 1
  const [page, setPage] = useState(1);
  // set số sản phẩm trên mỗi trang là 8
  const limit = 8;
  // sản phẩm bắt đầu từ vị trí 0 trong data trả về từ api
  const [noPage, setNoPage] = useState(0);

  // hàm thay đổi khi load trang set lại value cho page
  const changeHandler = (event, value) => {
    setPage(value);
  };

  // thực hiện phân trang
  useEffect(() => {
    const pagiantion = () => {
      setNoPage(Math.ceil(products.length / limit));
      let result = products.slice((page - 1) * limit, page * limit);
      setProductList(result);
    };
    pagiantion();
  }, [page, products]);

  const { key, name } = useParams();
  const history = useHistory();
  const dispatch = useDispatch();
  // chuyển đổi ra dạng số có dấu phẩy
  let decimalNumber = new Intl.NumberFormat("en-US");

  // thực hiện làm tròn số để loại bỏ số thập phân
  const randomNumber = () => {
    return 1000 + Math.trunc(Math.random() * 9000);
  };

  // gọi api để load sp về
  useEffect(() => {
    const callAPIProduct = async () => {
      try {
        const productDB = await axios.get("http://localhost:5000/products");
        //console.log(productDB.data);
        setProduct(productDB.data.Product);

        //nếu từ khóa tìm là all thì trả load ra toàn bộ sản phẩm
        if (key === "all" && name === "all") {
          let filterDB = productDB.data.Product;

          setProduct(filterDB);
          return;
        }

        // thực hiện lọc
        // nếu mà key tìm kiếm là all hoặc name thì trả ra tất cả
        if (key === "name" || key === "all") {
          let filterDB = productDB.data.Product.filter((data) => {
            return data.productName.toLowerCase().includes(name.toLowerCase());
          });
          setProduct(filterDB);
          return;
        }

        // nếu key tìm kiếm là giá min thì trả ra sp có giá tương ứng
        if (key === "pricemin" || key === "all") {
          let filterDB = productDB.data.Product.filter((data) => {
            return data.priceSale <= Number(name);
          });
          setProduct(filterDB);
          return;
        }

        // nếu key tìm kiếm là giá max thì trả ra sp có giá tương ứng
        if (key === "pricemax" || key === "all") {
          let filterDB = productDB.data.Product.filter((data) => {
            return data.priceSale <= Number(name);
          });
          setProduct(filterDB);
          return;
        }
      } catch (err) {
        console.log(err.message);
      }
    };

    callAPIProduct();
  }, [key, name]);

  // hàm để add sản phẩm vào giỏ hàng
  const onChangeAddToCart = (product) => {
    const productCart = {
      idPro: randomNumber(),
      product: product,
    };
    const action = addProduct(productCart);
    dispatch(action);
  };

  // hàm này thực hiện việc khi click vào button thi về lại trang chủ
  const backToHome = () => {
    history.push("/");
  };
  const detailUrl = "/detail/";
  return (
    <div>
      <Header />
      <Container>
        <div className="container-all-products">
          {/* nếu mà products.length === 0 thì hiển thị thông báo ko tìm thấy products */}
          {products.length === 0 ? (
            <div className="product-null">Product not found</div>
          ) : (
            // còn nếu có product thì dòng vòng lặp để duyệt các phần tử của mảng data trả về để hiển thị sp ra giao diện cart
            <div className="cards">
              {productList.map((element, index) => (
                <div className="card" key={index}>
                  <Link className="card-link" to={detailUrl + element._id}>
                    <img
                      src={element.productImage}
                      alt=""
                      className="card-image"
                    />
                  </Link>
                  <div className="card-content">
                    <div className="card-top">
                      <Link className="card-link" to={detailUrl + element._id}>
                        <h3 className="card-title">{element.productName}</h3>
                      </Link>
                      <div className="card-product">
                        {/* <img
                          src={element.productImage}
                          alt=""
                          className="card-product-avatar"
                        /> */}
                        <div className="card-product-info">
                          <div className="card-product-top">
                            <h4 className="card-product-name">
                              {element.productName}
                            </h4>
                          </div>
                          <div className="card-product-type">
                            <h6> {element.productType}</h6>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card-bottom">
                      <div>
                        <button
                          style={{ borderRadius: "10px" }}
                          className="btn btn-primary "
                          onClick={() => onChangeAddToCart(element)}
                        >
                          <span
                            className="fas fa-cart-plus"
                            style={{ margin: 5 }}
                          />
                          <span>
                            {decimalNumber.format(element.priceSale)} VND
                          </span>
                        </button>
                      </div>
                      <div className="card-price">
                        {decimalNumber.format(element.productPrice)} VND
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          )}
        </div>
        <div className="all-product-pagination">
          <div>
            <Grid
              /* container marginTop={5} marginBottom={5} */ justifyContent="flex-end"
            >
              <Stack /* spacing={2} */>
                <Pagination
                  onChange={changeHandler}
                  count={noPage}
                  defaultPage={page}
                  color="primary"
                />
              </Stack>
            </Grid>
          </div>
        </div>
      </Container>
      <div className="home">
        <button className="btn-home" onClick={backToHome}>
          Back To Home
        </button>
      </div>
      <Footer />
    </div>
  );
};
export default AllProduct;
