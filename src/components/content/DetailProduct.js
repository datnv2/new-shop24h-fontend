import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import Header from "../header/Header";
import ListProduct from "./ListProduct";
import axios from "axios";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import Footer from "../footer/Footer";
import { addProduct } from "../../actions/cart";
export const DetailProduct = (props) => {
  const { id } = props.match.params;
  const history = useHistory();
  const dispatch = useDispatch();
  const [product, setProduct] = useState([]);
  const [imgIcon, setImg] = useState([]);
  const [iconIndex, setIconIndex] = useState(0);
  const [quantity, setQuantity] = useState(1);

  // chuyển đổi ra dạng số thập phân
  let decimalNumber = new Intl.NumberFormat("en-US");

  // thực hiện làm tròn số để loại bỏ số thập phân
  const randomNumber = () => {
    return 1000 + Math.trunc(Math.random() * 9000);
  };

  // gọi api để load thông tin của sản phẩm theo id
  useEffect(() => {
    const getProductByProductId = async () => {
      try {
        const productDB = await axios.get(
          "http://localhost:5000/products/" + id
        );
        setProduct(productDB.data.Product);
        setImg(productDB.data.Product.productImage);
        let scrollElement = document.getElementById("scroll");
        scrollElement.scrollIntoView();
      } catch (err) {
        console.log(err.message);
      }
    };
    getProductByProductId();
  }, [id]);

  // hàm này để thay đổi ảnh của sản phẩm theo id được click trong detail của sản phẩm
  const onChangeIcon = (e) => {
    let iconIndex = e.target.getAttribute("data-key");
    setIconIndex(iconIndex);
  };
  let today = new Date();

  let date =
    today.getDate() +
    "/" +
    parseInt(today.getMonth() + 1) +
    "/" +
    today.getFullYear();

  // hàm này check xem thông tin user đăng nhập không nếu chưa có thì yêu cầu user đăng nhập
  // và thực hiện thêm sp vào giỏ hàng
  const handleBtnBuyClick = () => {
    if (localStorage.name === null || localStorage.name === undefined) {
      history.push("/login");
    } else {
      let productDetail = product;
      productDetail.productQuantity = quantity;
      const productCart = {
        idPro: randomNumber(),
        product: productDetail,
      };
      const action = addProduct(productCart);
      dispatch(action);
    }
  };
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Header />
      <Container style={{ marginTop: 50 }}>
        <div className="detail-body">
          <div className="detail-content">
            <div className="detail-content-image-icon">
              {imgIcon ? <img alt="" src={imgIcon[iconIndex]} /> : ""}
            </div>
            <div className="detail-content-image-list-icon">
              {imgIcon
                ? imgIcon.map((element, index) => (
                    <img
                      key={index}
                      data-key={index}
                      alt=""
                      src={element}
                      onClick={(index) => {
                        onChangeIcon(index);
                      }}
                    />
                  ))
                : ""}
            </div>
          </div>
          <div className="detail-content-info-product">
            <h2>{product.productName}</h2>
            <p>
              <span>Type: </span>
              {product.productType}
            </p>
            <p>
              <span>Name: </span>
              {product.productName}
            </p>
            <p>
              <span>Price: </span>
              <span className="detail-content-info-product-price">
                {decimalNumber.format(product.productPrice)}
              </span>
            </p>
            <p>
              <span>Price Sale: </span>
              {decimalNumber.format(product.priceSale)}
            </p>
            <h3>Description</h3>
            <p>{product.productDetail}</p>
          </div>
          <div className="detail-content-description">
            <div className="time">
              <span>Today: {date}</span>
            </div>
            <div className="detail-order">
              <span style={{ paddingTop: "10px" }}>Quantity:</span>
              <button
                className="detail-order-minus"
                onClick={() => {
                  if (quantity > 1) {
                    setQuantity(quantity - 1);
                  } else {
                    setQuantity(1);
                  }
                }}
              >
                <span className="fas fa-minus-circle" />
              </button>
              <span style={{ paddingTop: "10px" }}> {quantity}</span>
              <button
                className="detail-order-plus"
                onClick={() => {
                  setQuantity(quantity + 1);
                }}
              >
                <span className="fas fa-plus-circle" />
              </button>
            </div>
            <div className="detail-order-money">
              <span>
                Total:&nbsp;
                {product.productPrice
                  ? decimalNumber.format(product.productPrice * quantity)
                  : 0}
              </span>
            </div>
            <div className="btn-cart">
              <button className="btn-add-to-cart" onClick={handleBtnBuyClick}>
                Thêm vào giỏ hàng
              </button>
            </div>
          </div>
        </div>
        <div>
          <h2>Các sản phẩm khác</h2>
        </div>
        <ListProduct />
      </Container>
      <Footer />
    </div>
  );
};
export default DetailProduct;
