import React from "react";
import CarouselProduct from "./CarouselProduct";
//import ListProduct from "./ListProduct";
//import AllProduct from "./AllProduct.js";
import ListProduct from "./ListProduct";
import { useHistory } from "react-router-dom";
export const ProductList = (props) => {
  const history = useHistory();

  // hàm này thực hiện việc khi click vào  View All sẽ ra trang list product có phân trang
  const onBtnViewAllClick = () => {
    history.push("/products/all/all");
  };
  return (
    <div>
      <CarouselProduct />
      <ListProduct />
      <div className="view-all">
        <button className="btn-view-all" onClick={onBtnViewAllClick}>
          View All
        </button>
      </div>
    </div>
  );
};
export default ProductList;
