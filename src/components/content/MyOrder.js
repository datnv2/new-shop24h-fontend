import React, { useEffect, useState } from "react";
import Footer from "../footer/Footer.js";
import Header from "../header/Header.js";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { Button } from "react-bootstrap";
import {
  Container,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
} from "@mui/material";
import ModalMyOrderDetail from "./ModalOrderDetail/ModalMyOrderDetail.js";

export const MyOrder = () => {
  const history = useHistory();
  //danh sach don hang theo id của từng khách hàng
  const [myOrder, setMyOrder] = useState([]);

  // set trạng thái để mở modal order của user lên
  const [show, setShow] = useState(false);

  // hàm thực hiện đóng modal lại
  const handleCloseModal = () => {
    setShow(false);
  };

  // hàm thực hiện mở modal lên
  const handleShowModal = () => {
    setShow(true);
  };
  //ban đầu gán cho order detail là 1 mảng rỗng
  const [orderDetail, setOrderDetail] = useState([]);

  // gọi api để lấy ra list order của user theo id trên localStorage đăng nhập
  useEffect(() => {
    const loadAllOrderByCustomerId = async () => {
      const orderList = await axios.get(
        "http://localhost:5000/order/" + localStorage.getItem("id")
      );
      if (orderList.data.Order !== null) {
        setMyOrder(orderList.data.Order);
        //console.log(orderList);
      }
    };
    loadAllOrderByCustomerId();
  }, []);

  // hàm để lấy ra toàn bộ detail order
  const handleBtnDetailClick = async (Order) => {
    const orderDetailList = await axios.get(
      "http://localhost:5000/order-detail-id/" + Order._id
    );
    setOrderDetail(orderDetailList.data.orderDetail);
    handleShowModal();
  };
  // hàm này thực hiện việc click để quay về trang chủ
  const combackHome = () => {
    history.push("/");
  };

  return (
    <div>
      <Header />

      <Container /* className="pb-5 p-5 mt-5 mb-5" */
      /* style={{ marginTop: 180 }} */
      >
        <h1 style={{ textAlign: "center", color: "blue", marginTop: 20 }}>
          List Order User
        </h1>
        <Grid container>
          <TableContainer
            component={Paper}
            // style={{ border: "1px solid #f7e7ce" }}
          >
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell
                    align="center"
                    style={{ border: "1px solid #f7e7ce" }}
                  >
                    STT
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{ border: "1px solid #f7e7ce" }}
                  >
                    Order Date
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{ border: "1px solid #f7e7ce" }}
                  >
                    Required Date
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{ border: "1px solid #f7e7ce" }}
                  >
                    Shipped Date
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{ border: "1px solid #f7e7ce" }}
                  >
                    Note
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{ border: "1px solid #f7e7ce" }}
                  >
                    Status
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{ border: "1px solid #f7e7ce" }}
                  >
                    Action
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody style={{ border: "1px solid #f7e7ce" }}>
                {myOrder.map((element, index) => (
                  <TableRow
                    key={index}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell
                      component="th"
                      scope="row"
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      {index + 1}
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      {element.orderDate?.slice(0, 10)}
                    </TableCell>
                    <TableCell
                      style={{ border: "1px solid #f7e7ce" }}
                      align="center"
                    >
                      {element.requiredDate?.slice(0, 10)}
                    </TableCell>
                    <TableCell
                      style={{ border: "1px solid #f7e7ce" }}
                      align="center"
                    >
                      {element.shippedDate?.slice(0, 10)}
                    </TableCell>
                    <TableCell
                      style={{ border: "1px solid #f7e7ce" }}
                      align="center"
                    >
                      {element.note}
                    </TableCell>
                    <TableCell
                      style={{ border: "1px solid #f7e7ce" }}
                      align="center"
                    >
                      {element.status}
                    </TableCell>
                    <TableCell align="center">
                      <Button
                        onClick={combackHome}
                        variant="success"
                        style={{ borderRadius: "10px", textAlign: "center" }}
                      >
                        Quay lại
                      </Button>
                      <Button
                        variant="danger"
                        style={{ borderRadius: "10px", margin: 5 }}
                        onClick={() => handleBtnDetailClick(element)}
                      >
                        Chi tiết
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        {/* <Table striped bordered hover style={{ marginBottom: 155 }}>
          <thead>
            <tr style={{ textAlign: "center" }}>
              <th>STT</th>
              <th>Order Date</th>
              <th>Required Date</th>
              <th>Shipped Date</th>
              <th>Note</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {myOrder.map((element, index) => (
              <tr key={index} style={{ textAlign: "center" }}>
                <td>{index + 1}</td>
                <td>{element.orderDate?.slice(0, 10)}</td>
                <td>{element.requiredDate?.slice(0, 10)}</td>
                <td>{element.shippedDate?.slice(0, 10)}</td>
                <td>{element.note}</td>
                <td>{element.status}</td>
                <td>
                  <Button
                    onClick={combackHome}
                    variant="success"
                    style={{ borderRadius: "10px" }}
                  >
                    Quay lại
                  </Button>
                  <Button
                    variant="danger"
                    style={{ borderRadius: "10px", margin: 5 }}
                    onClick={() => handleBtnDetailClick(element)}
                  >
                    Chi tiết
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table> */}
      </Container>
      <Footer />
      <ModalMyOrderDetail
        show={show}
        orderDetail={orderDetail}
        handleClose={handleCloseModal}
        backHome={combackHome}
      />
    </div>
  );
};
