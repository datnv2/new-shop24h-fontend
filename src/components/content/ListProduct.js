import React, { useEffect, useState } from "react";
import axios from "axios";
import { Container } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { addProduct } from "../../actions/cart";
export const ListProduct = (props) => {
  // thực hiện làm tròn số để loại bỏ số thập phân
  const randomNumber = () => {
    return 1000 + Math.trunc(Math.random() * 9000);
  };

  //set products là 1 mảng rỗng để gán lại giá trị sau khi gọi api
  const [products, setProduct] = useState([]);
  const dispatch = useDispatch();

  // chuyển đổi ra dạng số thập phân
  let decimalNumber = new Intl.NumberFormat("en-US");

  // thực hiện gọi api để load ra số sp ở trang home
  useEffect(() => {
    const callAPIProduct = async () => {
      try {
        const productDB = await axios.get(
          "http://localhost:5000/all-products?page=" + 1
        );
        setProduct(productDB.data.Product);
      } catch (err) {
        console.log(err.message);
      }
    };
    callAPIProduct();
  }, []);

  // hàm này thực hiện add sp vào giỏ hàng trên trang mà không cần bấm vào detail của sp rồi mới add
  const onChangeAddToCart = (product) => {
    const productCart = {
      idPro: randomNumber(),
      product: product,
    };
    const action = addProduct(productCart);
    dispatch(action);
  };
  const detailUrl = "/detail/";
  return (
    <Container>
      <div className="cards">
        {products.map((element, index) => (
          <div className="card" key={index}>
            <Link className="card-link" to={detailUrl + element._id}>
              <img
                src={element.productImage[0]}
                alt=""
                className="card-image"
              />
            </Link>
            <div className="card-content">
              <div className="card-top">
                <Link className="card-link" to={detailUrl + element._id}>
                  <h3 className="card-title">{element.productName}</h3>
                </Link>
                <div className="card-product">
                  <img
                    src={element.productImage[1]}
                    alt=""
                    className="card-product-avatar"
                  />
                  <div className="card-product-info">
                    <div className="card-product-top">
                      <h4 className="card-product-name">
                        {element.productName}
                      </h4>
                    </div>
                    <div className="card-product-type">
                      <h5> {element.productType}</h5>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-bottom">
                <button
                  style={{ borderRadius: "10px" }}
                  className="btn btn-primary"
                  // className="card-sale"
                  onClick={() => onChangeAddToCart(element)}
                >
                  <span className="fas fa-cart-plus" style={{ margin: 5 }} />
                  <span>{decimalNumber.format(element.priceSale)} VND</span>
                </button>
                <div className="card-price">
                  <h6>{decimalNumber.format(element.productPrice)} VND</h6>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </Container>
  );
};
export default ListProduct;
