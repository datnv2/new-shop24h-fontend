import React from "react";
import { Container } from "react-bootstrap";
import avatar from "../../assets/avatar.jpg";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebook,
  faInstagram,
  faTwitter,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons";
export const Footer = () => {
  return (
    <Container fluid style={{ backgroundColor: "black", marginTop: 70 }}>
      <Container>
        <div className="customer-body">
          <div className="footer-container">
            <div className="footer-info">
              <ul>
                <li>
                  <h4>PRODUCTS</h4>
                </li>
                <li>
                  <p>Help Center</p>
                </li>
                <li>
                  <p>Contact Us</p>
                </li>
                <li>
                  <p>Product Help</p>
                </li>
                <li>
                  <p>Warranty</p>
                </li>
                <li>
                  <p>Order Status</p>
                </li>
              </ul>
            </div>
            <div className="footer-service">
              <ul>
                <li>
                  <h4>SERVICES</h4>
                </li>
                <li>
                  <p>Help Center</p>
                </li>
                <li>
                  <p>Contact Us</p>
                </li>
                <li>
                  <p>Product Help</p>
                </li>
                <li>
                  <p>Warranty</p>
                </li>
                <li>
                  <p>Order Status</p>
                </li>
              </ul>
            </div>
            <div className="footer-connect">
              <ul>
                <li>
                  <h4>SUPPORT</h4>
                </li>
                <li>
                  <p>Help Center</p>
                </li>
                <li>
                  <p>Contact Us</p>
                </li>
                <li>
                  <p>Product Help</p>
                </li>
                <li>
                  <p>Warranty</p>
                </li>
                <li>
                  <p>Order Status</p>
                </li>
              </ul>
            </div>
            <div className="footer-logo">
              <Link to="/">
                <img
                  alt=""
                  src={avatar}
                  style={{
                    width: "150px",
                    // marginTop: 20,
                    // marginBottom: 20,
                    borderRadius: "10px",
                  }}
                />
              </Link>
              <div className="footer-icons">
                <FontAwesomeIcon icon={faFacebook} />
                <FontAwesomeIcon icon={faInstagram} />
                <FontAwesomeIcon icon={faYoutube} />
                <FontAwesomeIcon icon={faTwitter} />
              </div>
            </div>
          </div>
        </div>
      </Container>
    </Container>
  );
};
export default Footer;
