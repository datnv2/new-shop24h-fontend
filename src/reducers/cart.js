const initialState = {
  listproduct: [],
  activeId: null,
};

// hàm này thực hiện việc thêm, xóa sp, tăng, giảm sp khi hành động tương ứng được click
const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_PRODUCT": {
      let result = false;
      for (let i = 0; i < state.listproduct.length; i++) {
        if (action.payload.product._id === state.listproduct[i].product._id) {
          state.listproduct[i].product.productQuantity += 1;
          result = true;
        }
      }
      if (result === true) {
        return {
          ...state,
          listproduct: [...state.listproduct],
        };
      }
      return {
        ...state,
        listproduct: [...state.listproduct, action.payload],
      };
    }
    case "REMOVE_PRODUCT": {
      const newList = [...state.listproduct];
      const result = newList.filter(
        (product) => product.idPro !== action.payload
      );
      return {
        ...state,
        listproduct: result,
      };
    }
    case "INCREMENT_PRODUCT": {
      for (let i = 0; i < state.listproduct.length; i++) {
        if (action.payload.product._id === state.listproduct[i].product._id) {
          state.listproduct[i].product.productQuantity += 1;
        }
      }
      return {
        ...state,
        listproduct: [...state.listproduct],
      };
    }
    case "DECREMENT_PRODUCT": {
      if (action.payload.product.productQuantity > 1) {
        for (let i = 0; i < state.listproduct.length; i++) {
          if (action.payload.product._id === state.listproduct[i].product._id) {
            state.listproduct[i].product.productQuantity =
              state.listproduct[i].product.productQuantity - 1;
          }
        }
        return {
          ...state,
          listproduct: [...state.listproduct],
        };
      } else {
        const newList = [...state.listproduct];
        const result = newList.filter(
          (product) => product._id !== action.payload._id
        );
        return {
          ...state,
          listproduct: result,
        };
      }
    }
    case "DELETE_ALL": {
      return {
        listproduct: [],
      };
    }

    default: {
      return state;
    }
  }
};
export default cartReducer;
