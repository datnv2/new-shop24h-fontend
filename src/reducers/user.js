const initialState = {
  user: [],
  activeId: null,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_USER": {
      const newList = action.payload;
      return {
        ...state,
        user: newList,
      };
    }
    default: {
      return state;
    }
  }
};
export default userReducer;
