import { initializeApp } from "firebase/app";
import axios from "axios";
import {
  getAuth,
  GoogleAuthProvider,
  signInWithPopup,
  signOut,
} from "firebase/auth";

// phần key để kết nối với firebase khi đăng nhập
const firebaseConfig = {
  apiKey: "AIzaSyDSvfY5pTpN6SgsLA7eo9bLWlnm4jdar2s",
  authDomain: "fir-332c4.firebaseapp.com",
  projectId: "fir-332c4",
  storageBucket: "fir-332c4.appspot.com",
  messagingSenderId: "764080791961",
  appId: "1:764080791961:web:05a0bd1d1937910d2c3308",
  measurementId: "G-B8DJVJ5X3R",
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const authOut = getAuth();
const provider = new GoogleAuthProvider();
// thực hiện đăng nhập firebase với google
export const signInWithGoogle = () => {
  signInWithPopup(auth, provider)
    .then(async (result) => {
      const emailFil = await axios.get(
        "http://localhost:5000/customer-email?email=" + result.user.email
      );
      if (emailFil.data.customer) {
        const name = emailFil.data.customer.fullName;
        const id = emailFil.data.customer._id;
        const email = emailFil.data.customer.email;
        const profilePic = emailFil.data.customer.photoURL;
        const address = emailFil.data.customer.address;
        const phone = emailFil.data.customer.phone;
        localStorage.setItem("name", name);
        localStorage.setItem("id", id);
        localStorage.setItem("email", email);
        localStorage.setItem("profilePic", profilePic);
        localStorage.setItem("phone", phone);
        localStorage.setItem("address", address);
        window.location.replace("http://localhost:3000");
      } else {
        const name = result.user.displayName;
        const email = result.user.email;
        localStorage.setItem("name", name);
        localStorage.setItem("email", email);
        window.location.replace("http://localhost:3000/create-account");
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

// hàm này thực hiện việc logout google
export const signOutWithGoogle = () => {
  signOut(authOut)
    .then(() => {
      localStorage.removeItem("name");
      localStorage.removeItem("email");
      localStorage.removeItem("profilePic");
      window.location.replace("http://localhost:3000/");
    })
    .catch((error) => {});
};
