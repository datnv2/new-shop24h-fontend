import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import HomePage from "./components/HomePage";
import DetailProduct from "./components/content/DetailProduct";
import Login from "./components/header/Login";
import CreateAccount from "./components/header/CreateAccount";
import ShoppingCart from "./components/header/ShoppingCart";
import CustomerInfo from "./components/header/CustomerInfo";
import AllProduct from "./components/content/AllProduct";
import { MyOrder } from "./components/content/MyOrder";
export const HomePageRouter = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/detail/:id" component={DetailProduct} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/create-account" component={CreateAccount} />
        <Route exact path="/shopping-cart" component={ShoppingCart} />
        <Route exact path="/customer-info" component={CustomerInfo} />
        <Route path="/products/:key/:name" component={AllProduct} />
        <Route exact path="/my-order" component={MyOrder} />
      </Switch>
    </Router>
  );
};
export default HomePageRouter;
