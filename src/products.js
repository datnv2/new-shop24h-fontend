const products = [
  {
    _id: "62125ff07dccefd42a30a1c3",
    name: "Đồng hồ nam Curren 8291",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/fc/e6/14/36e7ca7664ab7fad68d8d92f38b5f00d.jpg",
    buyPrice: 590000,
    promotionPrice: 500000,
    description:
      "Đồng hồ nam dây da thời trang phong cách Hàn Quốc cực đẹp, kích thước mặt : 32x25mm, chống nước sinh hoạt, kích thước dây : 1.8cm",
    timeCreated: "2022-02-20T15:36:16.987Z",
    timeUpdated: "2022-02-20T15:36:16.987Z",
    __v: 0,
  },
  {
    _id: "6212604c7dccefd42a30a1c5",
    name: "Đồng hồ OEM ",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/f9/0a/27/3875877b6626f50a43cf8a84a899da52.jpg",
    //"https://salt.tikicdn.com/cache/w1200/ts/product/95/55/b5/b9962cf826e01af38233cd810d5ebb60.jpg",
    //cũ"https://salt.tikicdn.com/cache/w1200/ts/product/57/68/9e/4900e0eda9841bf8521a38f562f27625.jpg",
    buyPrice: 590000,
    promotionPrice: 500000,
    description:
      "Đồng hồ nam dây da thời trang phong cách Hàn Quốc cực đẹp, kích thước mặt : 32x25mm, chống nước sinh hoạt, kích thước dây : 1.8cm",
    timeCreated: "2022-02-20T15:37:48.709Z",
    timeUpdated: "2022-02-20T15:37:48.709Z",
    __v: 0,
  },
  {
    _id: "621260777dccefd42a30a1c7",
    name: "Đồng hồ Garmin ",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/ac/fb/ab/0eb5927025176e4a4dbf6f89c64fb788.jpg",
    buyPrice: 590000,
    promotionPrice: 500000,
    description:
      "Đồng hồ nam dây da thời trang phong cách Hàn Quốc cực đẹp, kích thước mặt : 32x25mm, chống nước sinh hoạt, kích thước dây : 1.8cm",
    timeCreated: "2022-02-20T15:38:31.317Z",
    timeUpdated: "2022-02-20T15:38:31.317Z",
    __v: 0,
  },
  {
    _id: "621260957dccefd42a30a1c9",
    name: "Đồng hồ nam MF0218G",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/2b/7d/4b/883b965f4954e34c59f73b53abaa2078.jpg",
    //"https://salt.tikicdn.com/cache/w1200/ts/product/b7/a9/15/074af9ea960f63bc978b98845354214f.jpg",
    //"https://salt.tikicdn.com/cache/w1200/ts/product/2c/78/1d/0b1abe4efd80e17e798ef8d3aaf4bd11.jpg",
    //"	https://salt.tikicdn.com/cache/w1200/ts/product/7f/9b/1f/2d005af37664671e90caf318ee773584.PNG",
    //"https://salt.tikicdn.com/cache/w1200/ts/product/39/ca/7d/71d52e1197b134c37993a75988bbc3e2.png",
    //"https://salt.tikicdn.com/cache/w1200/ts/product/e1/24/61/a023c63aeb258f6363c7a13945a5e87c.jpg",
    //"https://salt.tikicdn.com/cache/w1200/ts/product/f9/0a/27/3875877b6626f50a43cf8a84a899da52.jpg",
    // "https://salt.tikicdn.com/cache/w1200/ts/product/3d/dc/8a/2fa69a19928437202a85608b2cadd60c.jpg",
    //"https://salt.tikicdn.com/cache/w1200/ts/product/5d/02/47/70202377ea277853cd25b9a1c36f165e.jpg",
    // "https://salt.tikicdn.com/cache/w1200/ts/product/24/4d/d0/bb716d65e1b8593fb2775ad2d0eb3e3f.jpg",
    buyPrice: 699000,
    promotionPrice: 600000,
    description:
      "Đồng hồ đeo tay chống nước dây thép không gỉ dành cho nam MINI FOCUS MF0218G  cao cấp doanh nhân lịch lãm ZO102 Kích thước mặt : 32x25mm, kích thước dây : 1.8cm, loại dây đeo: dây kim loại cao cấp",
    timeCreated: "2022-02-20T15:39:01.706Z",
    timeUpdated: "2022-02-20T15:39:01.706Z",
    __v: 0,
  },
  {
    _id: "621260a67dccefd42a30a1cb",
    name: "Đồng Hồ Thông Minh AMA w1200ts",
    type: "watch",
    imageUrl:
      "	https://salt.tikicdn.com/cache/w1200/ts/product/1a/09/39/0b1974d34e2687a23c06730bcf194bf9.jpg",
    // "https://salt.tikicdn.com/cache/w1200/ts/product/34/c4/3b/814f225accd95cf3de7c98a848ae9bb5.jpg",
    //"https://salt.tikicdn.com/cache/w1200/ts/product/ed/c9/ee/2afc0dd695e5d245c6e6c84d5150f244.jpg",
    //cũ"https://salt.tikicdn.com/cache/w1200/ts/product/2b/7d/4b/883b965f4954e34c59f73b53abaa2078.jpg",
    //"https://salt.tikicdn.com/cache/w1200/ts/product/7f/83/49/54bc2a389ab7fdfdf4567ae92be84358.png",
    // "https://salt.tikicdn.com/cache/w1200/ts/product/60/d4/d2/f7eae7468d71bad8f3a2c606cc3fcd79.jpg",
    buyPrice: 1149000,
    promotionPrice: 1000000,
    description:
      "Đồng Hồ Thông Minh AMA Watch Nam Nữ chống nước | Gọi điện thoại Xem tin nhắn Theo dõi Sức khỏe vận động Thiết kế nhỏ gọn Thời trang Hàng nhập khẩu",
    timeCreated: "2022-02-20T15:39:18.473Z",
    timeUpdated: "2022-02-20T15:39:18.473Z",
    __v: 0,
  },
  {
    _id: "621260c07dccefd42a30a1cd",
    name: "Đồng hồ AMA HS6621 Mặt Kính Chống Nước, Trầy Xước",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/54/41/c4/f09534af4fa78abe12fa8bd6fd43591e.jpg",
    buyPrice: 650000,
    promotionPrice: 500000,
    description:
      "Đồng hồ nam Chất liệu: Kim loại + ABS + Cao su thời trang phong cách cực đẹp, kích thước mặt : 32x25mm, chống nước sinh hoạt, kích thước dây : 1.8cm",
    timeCreated: "2022-02-20T15:39:44.416Z",
    timeUpdated: "2022-02-20T15:39:44.416Z",
    __v: 0,
  },
  {
    _id: "621260cf7dccefd42a30a1cf",
    name: "Đồng hồ nam Casio MTP-VT01GL-1B2UDF",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/86/47/67/681098050f3701193bf752bb03eeac95.jpg",
    buyPrice: 935360,
    promotionPrice: 900000,
    description:
      "Đồng hồ nam dây da Casio Standard chính hãng MTP-VT01GL-1B2UDF (40mm), Vỏ mạ ion màu vàng, Chống nước",
    timeCreated: "2022-02-20T15:39:59.706Z",
    timeUpdated: "2022-02-20T15:39:59.706Z",
    __v: 0,
  },
  {
    _id: "621260e17dccefd42a30a1d1",
    name: "Bingo Target Classic Brown BGT-C-BR",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/e1/e9/e9/a29d3bd7d6c52e81a2e8b7f458008033.png",
    buyPrice: 1492500,
    promotionPrice: 1300000,
    description:
      "Bingo Target Classic Brown, chất liệu dây thép không gỉ, kích thước mặt : 40mm, kích cỡ dây : 20mm, chống nước : 3ATM",
    timeCreated: "2022-02-20T15:40:17.979Z",
    timeUpdated: "2022-02-20T15:40:17.979Z",
    __v: 0,
  },
  {
    _id: "621260f07dccefd42a30a1d3",
    name: "Đồng hồ nam Casio Edifice ECB-20CL-1ADF",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/5f/fc/bc/e38627e730e8a00e173f5aafbd8bae21.jpg",
    buyPrice: 4310000,
    promotionPrice: 4000000,
    description:
      "Đồng hồ nam dây da Casio Edifice chính hãng ECB-20CL-1ADF (46mm), kết nối điện thoại thông minh từ EDIFICE,  khả năng chống nước ở độ sâu 100 mét",
    timeCreated: "2022-02-20T15:40:32.165Z",
    timeUpdated: "2022-02-20T15:40:32.165Z",
    __v: 0,
  },
  {
    _id: "621260fc7dccefd42a30a1d5",
    name: "Đồng hồ nam HAZEAL H1323-2",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/1a/df/6d/62c676715b703255cf201344d6f8eb87.png",
    buyPrice: 3250000,
    promotionPrice: 31000000,
    description:
      "Đồng hồ nam HAZEAL H1323-2 chính hãng Thụy Sỹ, chất liệu: Thép không gỉ 316L, Chống nước 5ATM, Kính sapphire, Thời gian trữ cót lâu lên đến 40h",
    timeCreated: "2022-02-20T15:40:44.482Z",
    timeUpdated: "2022-02-20T15:40:44.482Z",
    __v: 0,
  },
  {
    _id: "6212610c7dccefd42a30a1d7",
    name: "Đồng hồ Lobinni L2065-1 Chính hãng Thụy Sỹ",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/1f/bc/8d/2c2d3ccedc9c3f9f744082d21c65629d.png",
    buyPrice: 4860000,
    promotionPrice: 4350000,
    description:
      "Đồng hồ Lobinni L2065-1 Chính hãng Thụy Sỹ, Chất liệu vỏ Thép không gỉ 316L, Dây da cao cấp, mặt kính Sapphire",
    timeCreated: "2022-02-20T15:41:00.587Z",
    timeUpdated: "2022-02-20T15:41:00.587Z",
    __v: 0,
  },
  {
    _id: "6212611d7dccefd42a30a1d9",
    name: "Đồng hồ AMA PHY6202 Đo nhịp tim Huyết áp ",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/49/0b/3a/e05db512c5fe77b0393247c705636114.jpg",
    //"	https://salt.tikicdn.com/cache/w1200/ts/product/2a/62/dc/73e9fb8164099d2b4d4da89c2636e98b.jpg",
    //"https://salt.tikicdn.com/cache/w1200/ts/product/1a/09/39/0b1974d34e2687a23c06730bcf194bf9.jpg",
    //"https://salt.tikicdn.com/cache/400x400/ts/product/2a/1f/27/92606946d09fa1bd0fe5a2a569d0cca5.png",
    //"https://salt.tikicdn.com/cache/w1200/ts/product/cc/49/d2/73098db4531385eb0e4488af06e85dcd.jpg",
    // "https://salt.tikicdn.com/cache/w1200/ts/product/e3/c3/b5/65eaf082daaa5582bfbaab2a0a4615a2.jpg",
    //"	https://salt.tikicdn.com/cache/w1200/ts/product/82/0a/13/3ac15110fd0066bbfba3b9cf48721cd6.jpg",
    //"https://salt.tikicdn.com/cache/w1200/ts/product/a7/08/6b/124e45673b34353dec75d343559a90ee.jpg",
    //"https://salt.tikicdn.com/cache/w1200/ts/product/cc/49/d2/73098db4531385eb0e4488af06e85dcd.jpg",
    // "https://salt.tikicdn.com/cache/w1200/ts/product/e3/82/df/5c41004865f0e0af14f31e39a83369cd.jpg",
    buyPrice: 1547001,
    promotionPrice: 1300000,
    description:
      "Chip PHY6202 nền tảng PHY Tần Số 48Mhz Kiến Trúc ARM, Bo Mạch Chủ Bộ nhớ ROM 512KB SRAM 138KB falsh 32M Bổ Sung chip cảm biến G",
    timeCreated: "2022-02-20T15:41:17.971Z",
    timeUpdated: "2022-02-20T15:41:17.972Z",
    __v: 0,
  },
  {
    _id: "6212612f7dccefd42a30a1db",
    name: "Đồng Hồ Nam Citizen Dây Thép Không Gỉ BL1240-59E",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/fc/e6/ff/50979e29a6567a425bee567350f6d887.jpg",
    buyPrice: 9580000,
    promotionPrice: 9000000,
    description:
      "Đồng Hồ Nam Citizen Dây Thép Không Gỉ BL1240-59E - Mặt Đen (Sapphire), Kích thước mặt: 42 mm, Chống nước: 10 ATM (100M) thoải mái đi mưa, rửa tay, đi bơi (không dùng đi lặn)",
    timeCreated: "2022-02-20T15:41:35.044Z",
    timeUpdated: "2022-02-20T15:41:35.044Z",
    __v: 0,
  },
  {
    _id: "6212613d7dccefd42a30a1dd",
    name: "Đồng Hồ Nam Bulova Dây Da 97A138 ",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/6d/db/bb/318f42a7cfd554c94e65a71122277ec1.jpg",
    buyPrice: 13300000,
    promotionPrice: 13000000,
    description:
      "Đồng Hồ Nam Bulova Dây Da 97A138 - Mặt Trắng, 100% đồng hồ Bulova chính hãng thương hiệu Mỹ, Kích thước mặt: 43 mm, Chống nước: 3 ATM",
    timeCreated: "2022-02-20T15:41:49.283Z",
    timeUpdated: "2022-02-20T15:41:49.283Z",
    __v: 0,
  },
  {
    _id: "6212614b7dccefd42a30a1df",
    name: "Đồng Hồ Orient  RA-AA0004E19B",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/7f/90/53/e7ff195319ba67e1ec737c92ecec2c5f.jpg",
    buyPrice: 7356000,
    promotionPrice: 7000000,
    description:
      "Chip PHY6202 nền tảng PHY Tần Số 48Mhz Kiến Trúc ARM, Bo Mạch Chủ Bộ nhớ ROM 512KB SRAM 138KB falsh 32M Bổ Sung chip cảm biến G",
    timeCreated: "2022-02-20T15:42:03.321Z",
    timeUpdated: "2022-02-20T15:42:03.321Z",
    __v: 0,
  },
  {
    _id: "6212615a7dccefd42a30a1e1",
    name: "Đồng hồ nam 0052G",
    type: "watch",
    imageUrl:
      "https://salt.tikicdn.com/cache/w1200/ts/product/35/5d/ed/a67f3447768f195da0a4bf0b27383965.jpg",
    buyPrice: 4100000,
    promotionPrice: 3800000,
    description:
      "Đồng hồ nam 0052G MINI FOCUS chính hãng Lobinni No.14003, Đường kính mặt 39,5 mm, Chất liệu vỏ Thép không gỉ 316L, Chất liệu mặt kính Sapphire, chịu nước 5ATM",
    timeCreated: "2022-02-20T15:42:18.353Z",
    timeUpdated: "2022-02-20T15:42:18.353Z",
    __v: 0,
  },
];

export default products;
