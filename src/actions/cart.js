// hàm này thực hiện hành động add sản phẩm vào giỏ hàng
export const addProduct = (product) => {
  return {
    type: "ADD_PRODUCT",
    payload: product,
  };
};

// hàm này thực hiện việc hành động xóa sp ra khỏi giỏ hàng
export const removeProduct = (product) => {
  return {
    type: "REMOVE_PRODUCT",
    payload: product,
  };
};

// hàm này thực hiện hành động tăng sp lên trong giỏ hàng
export const incrementProduct = (product) => {
  return {
    type: "INCREMENT_PRODUCT",
    payload: product,
  };
};

// hàm này thực hiện giảm sp trong giỏ hàng xuống
export const decrementProduct = (product) => {
  return {
    type: "DECREMENT_PRODUCT",
    payload: product,
  };
};

// hàm này thực hiện xóa toàn bộ sp trong giỏ hàng
export const deleteAllProduct = (product) => {
  return { type: "DELETE_ALL", payload: product };
};
